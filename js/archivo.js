
if(!("tareas" in localStorage)){
traerListaTareas();
}

document.getElementById("textoModificar").setAttribute("disabled","disabled");
document.getElementById("textoModificar").value = "";

cargarTareas();
prendeleAlosEnters();
mostrarTareasRestantes("no");
mostrarTareasRestantes("si");

function prendeleAlosEnters(){

let input = document.getElementById("textoTarea");

input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    document.getElementById("btnAg").click();
  }
});


let inputMod = document.getElementById("textoModificar");

inputMod.addEventListener("keyup", function(event) {
	if(event.keyCode === 13) {
		event.preventDefault();
		document.getElementById("btnMod").click();
	}
})


}


function cargarTareas(){

let info = JSON.parse( localStorage.getItem( "tareas" ) )
if(info!==null){
for (var i = 0; i < info.length; i++) {
agregarTareaDelStorage(info[i])
}
}

}

function agregarEventoCerrar(cerrar, texto){

	
  cerrar.addEventListener("click", function() {
  let otro = JSON.parse( localStorage.getItem( "tareas" ) )  
  var div = this.parentElement;
    		div.style.display = "none";
		for(let h = 0; h<otro.length; h++ ){
			if(otro[h].tarea == texto){
		otro.splice(h,1);
		localStorage.setItem("tareas", JSON.stringify( otro ));
			}
			}
			mostrarTareasRestantes("no");
			mostrarTareasRestantes("si");
  });

}


function traerListaTareas(){
let auxStorageame = localStorage.getItem( "tareas" );
let auxInf = JSON.parse( auxStorageame === null ? "[]" : auxStorageame );

fetch('http://jsonplaceholder.typicode.com/todos?_start=0&_limit=10')
	.then(response => response.json())
	.then(function(json){

for(let i = 0; i<json.length; i++){
	let nuevoItem = document.createElement("li");
	nuevoItem.innerHTML = json[i].title;
	nuevoItem.setAttribute("value", json[i].title);
	nuevoItem.className = "elementoLista";
	let span = document.createElement("span");
	let txt = document.createTextNode("\u00D7");
	span.className = "cerrar";
	span.appendChild(txt);
	nuevoItem.appendChild(span);

	cambiarEstadoTarea(json[i].title, nuevoItem, span);
	agregarEventoCerrar(span, json[i].title);

	nuevoItem.addEventListener("contextmenu", function(ev){
			ev.preventDefault();
			let inputMod = document.getElementById("textoModificar");
			inputMod.value = json[i].title;
			inputMod.removeAttribute("disabled");
			document.getElementById("txtModEscondido").value = json[i].title;
	});
	document.getElementById("listaTareas").appendChild(nuevoItem);


	let tareas = {
		tarea: json[i].title,
		hecha: "no",
	}
	auxInf.push(tareas);
}

localStorage.setItem("tareas", JSON.stringify( auxInf ));
mostrarTareasRestantes("no");
			mostrarTareasRestantes("si");
});

	

}



function agregarTareaDelStorage(i){

	let nuevoItem = document.createElement("li");
	nuevoItem.innerHTML = i.tarea;
	nuevoItem.className = "elementoLista";
	nuevoItem.setAttribute("value", i.tarea);
	if(i.hecha == "si"){
		nuevoItem.innerHTML = "//"+nuevoItem.innerHTML;
		nuevoItem.setAttribute("class","tareaHecha");
	}
	let span = document.createElement("span");
  	let txt = document.createTextNode("\u00D7");
  	span.className ="cerrar";
  	span.appendChild(txt);
  	nuevoItem.appendChild(span);
  	document.getElementById("listaTareas").appendChild(nuevoItem);

  	cambiarEstadoTarea(i.tarea, nuevoItem, span);
	//esto es para el click,derecho, nuevo,
	nuevoItem.addEventListener("contextmenu", function(ev){
			ev.preventDefault();
			let inputMod = document.getElementById("textoModificar");
			inputMod.value = i.tarea;
			inputMod.removeAttribute("disabled");
			document.getElementById("txtModEscondido").value = i.tarea;
	});

	//hasta aca 

  	span.addEventListener("click", function(){
  		let otro = JSON.parse( localStorage.getItem( "tareas" ) )  
  		var div = this.parentElement;
    		div.style.display = "none";
  		for(let t = 0; t<otro.length; t++){
  			if(otro[t].tarea == i.tarea){			
  				otro.splice(t,1);
  				localStorage.setItem("tareas", JSON.stringify( otro ));
  				mostrarTareasRestantes("no");
  				mostrarTareasRestantes("si");
  			}
  		}
  	});

}


function cambiarEstadoTarea(tarea, elemento, span){
	elemento.ondblclick = function(){
		
		let auxStorageame = localStorage.getItem( "tareas" );
		let auxInf = JSON.parse( auxStorageame === null ? "[]" : auxStorageame );

		for(let y = 0; y<auxInf.length; y++){
			
			if(auxInf[y].tarea == tarea){
				if(auxInf[y].hecha == "no"){
				elemento.lastElementChild.remove();
			elemento.innerHTML = "//"+elemento.innerHTML;
			elemento.setAttribute("class","tareaHecha");
			elemento.appendChild(span);
			auxInf[y].hecha = "si";
		}else{
			elemento.lastElementChild.remove();
			elemento.innerHTML=elemento.innerHTML.substring(2);
			elemento.removeAttribute("class","tareaHecha");
			elemento.appendChild(span);
			auxInf[y].hecha = "no";
		}
				localStorage.setItem("tareas", JSON.stringify( auxInf ))
			}
		}
		mostrarTareasRestantes("no");
		mostrarTareasRestantes("si");
	}
	
}



function agregarTarea(){


	let storageame = localStorage.getItem( "tareas" );
	let info = JSON.parse( storageame === null ? "[]" : storageame );
	

	let textoTarea = document.getElementById("textoTarea").value;
	let nuevoItem = document.createElement("li");
	
	
	if(textoTarea == ""){
		alert("Tienes que escribir alguna tarea");
			document.getElementById("textoTarea").value = "";
	}else if(existeTarea(textoTarea)){
		alert("La tarea que querés ingresar ya existe");
			document.getElementById("textoTarea").value = "";
	}else{


	nuevoItem.innerHTML = textoTarea;
	nuevoItem.className = "elementoLista";
	nuevoItem.setAttribute("value", textoTarea);
	//nuevoItem.id = cantElementos;
	let span = document.createElement("span");
  	let txt = document.createTextNode("\u00D7");
  	span.className ="cerrar";
  	span.appendChild(txt);
  	nuevoItem.appendChild(span);
	document.getElementById("listaTareas").appendChild(nuevoItem);

	cambiarEstadoTarea(textoTarea, nuevoItem, span);

	agregarEventoCerrar(span, textoTarea);
	
	nuevoItem.addEventListener("contextmenu", function(ev){
			ev.preventDefault();
			let inputMod = document.getElementById("textoModificar");
			inputMod.value = textoTarea;
			inputMod.removeAttribute("disabled");
			document.getElementById("txtModEscondido").value = textoTarea;
	});
	
	
	let tareas = {
		tarea: textoTarea,
		hecha: "no",
	}
	console.log(info);
	info.push(tareas);
	localStorage.setItem("tareas", JSON.stringify( info ))
	document.getElementById("textoTarea").value = "";
	mostrarTareasRestantes("no");
	mostrarTareasRestantes("si");
	}

}

function modificarTarea(){
	let hecha = false;
	let txtTareaMod = document.getElementById("textoModificar").value;
	let txtOriginal = document.getElementById("txtModEscondido").value;
	let auxStorageame = localStorage.getItem( "tareas" );
		let auxInf = JSON.parse( auxStorageame === null ? "[]" : auxStorageame );
		if(existeTarea(txtTareaMod)){
			alert("Ya existe una tarea con este nombre");
			document.getElementById("textoModificar").value = "";
		}else{


		for(let i = 0; i < auxInf.length; i++){
			if(auxInf[i].tarea == txtOriginal){
				auxInf[i].tarea = txtTareaMod;
				if(auxInf[i].hecha == "si"){
					hecha = true;
				}
			}
		}

		let todosLi = document.getElementsByTagName("li");
		for(let x = 0; x < todosLi.length; x++){
			
			if(todosLi[x].getAttribute("value") == txtOriginal){

				if(hecha == true){
					todosLi[x].innerHTML ="//"+txtTareaMod;
					
				}else{
					todosLi[x].innerHTML = txtTareaMod;
					
				}
			todosLi[x].setAttribute("value",txtTareaMod);
			let span = document.createElement("span");
  			let txt = document.createTextNode("\u00D7");
  			span.className ="cerrar";
  			span.appendChild(txt);
  			todosLi[x].appendChild(span);
  			agregarEventoCerrar(span, txtTareaMod);
  			cambiarEstadoTarea(txtTareaMod, todosLi[x], span);

  			todosLi[x].addEventListener("contextmenu", function(ev){
			ev.preventDefault();
			let inputMod = document.getElementById("textoModificar");
			inputMod.value = txtTareaMod;
			inputMod.removeAttribute("disabled");
			document.getElementById("txtModEscondido").value = txtTareaMod;
			});

			}
		}


		localStorage.setItem("tareas", JSON.stringify( auxInf ));
		document.getElementById("textoModificar").value = "";
		}
}


function borrarHechas(){

	let storageameTodo = localStorage.getItem( "tareas" );

	var infoTodo = JSON.parse( storageameTodo === null ? "[]" : storageameTodo );
	let todasLi = document.getElementsByTagName("li");
	
let auxArr = [];
let auxArrHechas = [];

	auxArr = infoTodo.filter(function (obj){
		return obj.hecha == "no";
	});

	auxArrHechas = infoTodo.filter(function (obj){
		return obj.hecha == "si";
	});

	for(let i = 0; i < todasLi.length; i++){
		for(let k = 0; k < auxArrHechas.length; k++){
			if(auxArrHechas[k].tarea == todasLi[i].getAttribute("value")){
				todasLi[i].parentNode.removeChild(todasLi[i]);
			}
		}
	}
	
	localStorage.setItem("tareas", JSON.stringify( auxArr ));
	mostrarTareasRestantes("no");
	mostrarTareasRestantes("si");

}



function mostrarTareasRestantes(isHecha){

	let txtSpan = document.getElementById("txtTareasRestantes");
	let txtSpanRealizadas = document.getElementById("txtTareasRealizadas");
	let cantTareas = 0;

	let storageameTareas = localStorage.getItem( "tareas" );
	var infoTareas = JSON.parse( storageameTareas === null ? "[]" : storageameTareas );

	for(let i = 0; i < infoTareas.length; i++){
			if(infoTareas[i].hecha == isHecha){
				cantTareas += 1;
			}

	}
	if(isHecha == "no"){


	if(cantTareas == 0){
		txtSpan.innerHTML = "No tienes tareas pendientes";
	}else if(cantTareas == 1){
		txtSpan.innerHTML = "Queda "+cantTareas+" tarea";
	}else{
		txtSpan.innerHTML = "Quedan "+cantTareas+" tareas";
	}
	}else{
		if(cantTareas == 1){
			txtSpanRealizadas.innerHTML = cantTareas+" tarea hecha";
		}else if(cantTareas == 0){
			txtSpanRealizadas.innerHTML ="No tienes tareas hechas";
		}else{
			txtSpanRealizadas.innerHTML = cantTareas+" tareas hechas";
		}
		
	}
}


function mostrarInstrucciones() {

	var p=document.getElementsByClassName('pInstrucciones');
var cant=p.length;

for (var i=0; i<cant;i++){
	if(p[i].style.display=='none'){


    p[i].style.display='block';
    	}else{
     p[i].style.display='none';		
    	}
}
}


function existeTarea(textoTarea){
	let existe = false;
	let storageameExtra = localStorage.getItem( "tareas" );
	var infoExtra = JSON.parse( storageameExtra === null ? "[]" : storageameExtra );
	for (let i = 0; i < infoExtra.length; i++){
		if(infoExtra[i].tarea == textoTarea){
			existe = true;
		}
	}
	return existe;
}


